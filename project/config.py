import os
from dotenv import load_dotenv
import json 

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(os.path.dirname(basedir), '.env'))

class Config():
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.environ.get("DATABASE_URL")
    SESSION_COOKIE_SECURE = True
    SESSION_COOKIE_HTTPONLY = True
    SESSION_COOKIE_SAMESITE = 'None'
    MAX_CONTENT_LENGTH = 100 * 1024 * 1024
    MAIL_SERVER = os.environ.get("MAIL_SERVER")
    MAIL_PORT = os.environ.get("MAIL_PORT")
    MAIL_USE_TLS = os.environ.get("MAIL_USE_TLS")
    MAIL_USE_SSL = os.environ.get("MAIL_USE_SSL")
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME")
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD")
    MAIL_DEFAULT_SENDER = os.environ.get("MAIL_DEFAULT_SENDER")
    SECURITY_RECOVERABLE = True
    SECURITY_EMAIL_SENDER = os.environ.get("MAIL_SENDER")
    ALLOWED_USERS = json.loads(os.environ.get("ALLOWED_USERS"))

class ProductionConfig(Config):
    DEBUG = False
    FLASK_DEBUG = False
    SECRET_KEY = os.environ.get("PRODUCTION_SECRET_KEY")
    SECURITY_PASSWORD_SALT = os.environ.get("PRODUCTION_SECURITY_PASSWORD_SALT")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    GOOGLE_OAUTH_CLIENT_ID = os.environ.get("GOOGLE_OAUTH_CLIENT_ID")
    GOOGLE_OAUTH_CLIENT_SECRET = os.environ.get("GOOGLE_OAUTH_CLIENT_SECRET")
    
class DevelopmentConfig(Config):
    DEVELOPMENT = True
    FLASK_DEBUG = True
    DEBUG = True
    OAUTHLIB_INSECURE_TRANSPORT = True
    SECRET_KEY = os.environ.get("DEV_SECRET_KEY")
    SECURITY_PASSWORD_SALT = os.environ.get("DEV_SECURITY_PASSWORD_SALT")
    SQLALCHEMY_TRACK_MODIFICATIONS = True
